module.exports = function(RED) {

    function AlmStateMachine(config) {
        RED.nodes.createNode(this,config);
        var node = this;

        const AlarmModelKind = {
            event_only: 0,
            alarm_with_ack: 1,
            alarm_with_latch: 2,
            alarm_with_ack_and_latch: 3
        }

        const TmStmpMthKind = {
            calculation_based: 1,
            input_based: 2,
        }

        const AlarmStateKind = {
            norm: 0,
            unack: 1,
            acked: 2,
            rtnun: 3,
            lunack: 4,
            lacked: 5,
            shlvd: 6,
            dsuppr: 7,
            oosrv: 8
        }

        const AlarmCtlValueKind = {
            acknowledge: 0,
            reset: 1,
            shelve: 2,
            unshelve: 3,
            set_out_of_service: 4,
            set_in_service: 5,
            set_suppressed_by_design: 6,
            reset_suppressed_by_design: 7,
        }

        var AlarmData = 
        {
            "origin": {
              "orCat": "not-supported",
              "orIdent": 0
            },
            "ctlNum": 0,
            "stVal": 0,
            "Oper": {
              "ctlVal": "undefined",
              "operTm": {
                "FractionOfSecond": 680,
                "SecondSinceEpoch": 1600670577,
                "TimeQuality": {
                  "ClockFailure": false,
                  "ClockNotSyncronized": false,
                  "LeapSecondsKnown": true,
                  "TimeAccuracy": 10
                }
              },
              "origin": {
                "orCat": "not-supported",
                "orIdent": 0
              },
              "ctlNum": 0,
              "T": {
                "FractionOfSecond": 680,
                "SecondSinceEpoch": 1600670577,
                "TimeQuality": {
                  "ClockFailure": false,
                  "ClockNotSyncronized": false,
                  "LeapSecondsKnown": true,
                  "TimeAccuracy": 10
                }
              },
              "Test": false,
              "Check": 0
            },
            "q": {
              "detailQual": {
                "badReference": false,
                "failure": false,
                "inaccurate": false,
                "inconsistent": false,
                "oldData": false,
                "oscillatory": false,
                "outOfRange": false,
                "overflow": false
              },
              "operatorBlocked": false,
              "source": "process",
              "test": false,
              "validity": "good"
            },
            "t": {
              "FractionOfSecond": 683,
              "SecondSinceEpoch": 1600670577,
              "TimeQuality": {
                "ClockFailure": false,
                "ClockNotSyncronized": false,
                "LeapSecondsKnown": true,
                "TimeAccuracy": 10
              }
            },
            "stSeld": false,
            "opOk": false,
            "tOpOk": {
              "FractionOfSecond": 683,
              "SecondSinceEpoch": 1600670577,
              "TimeQuality": {
                "ClockFailure": false,
                "ClockNotSyncronized": false,
                "LeapSecondsKnown": true,
                "TimeAccuracy": 10
              }
            },
            "ctlModel": "status-only",
            "sboTimeout": 0,
            "sboClass": "operate-once",
            "operTimeout": 0,
            "d": "Dummy description 1",
            "dU": "Dummy description 1 unicode",
            "seqId": 0,
            "almAction": "Dummy action 1",
            "almModel": "event-only",
            "almMsg": "Dummy message 1",
            "almArea": "Dummy Area 1",
            "almPrio": 0,
            "onDlTmms": 0,
            "reAlmTmms": 0,
            "preTmms": 0,
            "pstTmms": 0,
            "smpTmms": 0,
            "tmStmpMth": "Calculation based",
            "setTm": {
              "FractionOfSecond": 683,
              "SecondSinceEpoch": 1600670577,
              "TimeQuality": {
                "ClockFailure": false,
                "ClockNotSyncronized": false,
                "LeapSecondsKnown": true,
                "TimeAccuracy": 10
              }
            },
            "setCal": {
              "occ": 0,
              "occType": "Time",
              "occPer": "Hour",
              "weekDay": "Monday",
              "month": "January",
              "day": 0,
              "hr": 0,
              "mn": 0
            },
            "subVal": "norm",
            "datSetRef": 0
        }

        var process_state = {val : false , dtcng : "none"};

        AlarmData.stVal = AlarmStateKind.norm;
        AlarmData.Oper.ctlVal = "undefined";
        AlarmData.almModel = AlarmModelKind[config.almmodel];

        if (config.almprio != null)
        {
            AlarmData.almPrio = Number(config.almprio);
        }
        else
        {
            AlarmData.almPrio = 1;
        }

        AlarmData.d = config.almd;
        AlarmData.almAction = config.almaction;
        AlarmData.almMsg = config.almmsg;
        AlarmData.almArea = config.almarea;

        node.on('input', function(msg) {
            if (msg.payload.Item == "any" && msg.payload.Value == "readall")
            {
                var msgout = msg;
                msgout.payload = {Item : this.name , Type : "ALM", Value : AlarmData};
                node.send(msgout);
            }
            else if (msg.payload.Item == this.name || msg.topic == "process")
            {
                switch (msg.topic)
                {
                    case "hmi" : 
                        AlarmData.Oper.ctlVal = msg.payload.Value.Oper.ctlVal;
                        process_state.dtcng = "none";
                        break;

                    case "process":
                        process_state.val = msg.payload;
                        if (process_state.val)
                        {
                            process_state.dtcng = "on";
                        }
                        else
                        {
                            process_state.dtcng = "off";
                        }
                        AlarmData.Oper.ctlVal = "undefined";

                        break;

                    default:
                        break;

                }

                if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.shelve)
                {
                    AlarmData.stVal = AlarmStateKind.shlvd;
                }

                if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.unshelve)
                {
                    if ( process_state.val == false)
                    {
                        AlarmData.stVal = AlarmStateKind.norm;
                    }
                    else
                    {
                    if (AlarmData.almModel == AlarmModelKind.event_only || AlarmData.almModel == AlarmModelKind.alarm_with_latch)
                    {
                        AlarmData.stVal = AlarmStateKind.acked;
                    }
                    if (AlarmData.almModel == AlarmModelKind.alarm_with_ack || AlarmData.almModel == AlarmModelKind.alarm_with_ack_and_latch)
                    {
                        AlarmData.stVal = AlarmStateKind.unack;
                    }
                    }
                    //ctlVal = "undefined";
                }

                if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.set_out_of_service)
                {
                    AlarmData.stVal = AlarmStateKind.oosrv;
                    //ctlVal = "undefined";
                }

                if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.set_in_service)
                {
                    if ( process_state.val == false)
                    {
                    AlarmData.stVal = AlarmStateKind.norm;
                    }
                    else
                    {
                    if (AlarmData.almModel == AlarmModelKind.event_only || AlarmData.almModel == AlarmModelKind.alarm_with_latch)
                    {
                        AlarmData.stVal = AlarmStateKind.acked;
                    }
                    if (AlarmData.almModel == AlarmModelKind.alarm_with_ack || AlarmData.almModel == AlarmModelKind.alarm_with_ack_and_latch)
                    {
                        AlarmData.stVal = AlarmStateKind.unack;
                    }
                    }
                    //ctlVal = "undefined";
                }

                if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.set_suppressed_by_design)
                {
                    AlarmData.stVal = AlarmStateKind.dsuppr;
                    //ctlVal = "undefined";
                }

                if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.reset_suppressed_by_design)
                {
                    if ( process_state.val == false)
                    {
                    AlarmData.stVal = AlarmStateKind.norm;
                    }
                    else
                    {
                    if (AlarmData.almModel == AlarmModelKind.event_only || AlarmData.almModel == AlarmModelKind.alarm_with_latch)
                    {
                        AlarmData.stVal = AlarmStateKind.acked;
                    }
                    if (AlarmData.almModel == AlarmModelKind.alarm_with_ack || AlarmData.almModel == AlarmModelKind.alarm_with_ack_and_latch)
                    {
                        AlarmData.stVal = AlarmStateKind.unack;
                    }
                    }
                    //ctlVal = "undefined";
                }

                //---------ALARM-WITH-ACK-------------
                if (AlarmData.almModel == AlarmModelKind.alarm_with_ack)
                {
                    switch (AlarmData.stVal)
                    {
                    case AlarmStateKind.norm: //8
                        if (process_state.dtcng == "on")
                        {
                            AlarmData.stVal = AlarmStateKind.unack;
                        }
                        break;
                    case AlarmStateKind.unack: //15
                        if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.acknowledge)
                        {
                            if ( process_state.val == true)
                            {
                                AlarmData.stVal = AlarmStateKind.acked;
                            }
                            else
                            {
                                AlarmData.stVal = AlarmStateKind.norm;
                            }
                        }
                        if (process_state.dtcng == "off")
                        {
                            AlarmData.stVal = AlarmStateKind.rtnun;
                        }
                        break;

                    case AlarmStateKind.acked: //11
                        if (process_state.dtcng == "off")
                        {
                        AlarmData.stVal = AlarmStateKind.norm;
                        }
                        break;

                    case AlarmStateKind.rtnun: //12
                        if (process_state.dtcng == "on")
                        {
                            AlarmData.stVal = AlarmStateKind.unack;
                        }
                        else
                        {
                            if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.acknowledge)
                            {
                                AlarmData.stVal = AlarmStateKind.norm;
                            }
                        }
                        break;
                    }
                }
                //-------------EVENT-ONLY--------------------
                if (AlarmData.almModel == AlarmModelKind.event_only)
                {
                    switch (AlarmData.stVal)
                    {
                    case AlarmStateKind.undefined:
                        AlarmData.stVal = AlarmStateKind.norm;
                        break;
                    case AlarmStateKind.norm:
                        if (process_state.dtcng == "on")
                        {
                        AlarmData.stVal = AlarmStateKind.acked;
                        }
                        break;
                    case AlarmStateKind.acked:
                        if (process_state.dtcng == "off")
                        {
                        AlarmData.stVal = AlarmStateKind.norm
                        }
                        break;
                    }
                }
                //------------ALARM-WITH-LATCH---------------------
                if (AlarmData.almModel == AlarmModelKind.alarm_with_latch)
                {
                    switch (AlarmData.stVal)
                    {
                    case AlarmStateKind.norm: //8
                        if (process_state.dtcng == "on")
                        {
                        AlarmData.stVal = AlarmStateKind.acked;
                        }
                        break;

                    case AlarmStateKind.acked: //11
                        if (process_state.dtcng == "off")
                        {
                        AlarmData.stVal = AlarmStateKind.lacked;
                        }
                        break;

                    case AlarmStateKind.lacked: //12
                        if (process_state.dtcng == "on")
                        {
                        AlarmData.stVal = AlarmStateKind.acked;
                        }
                        else
                        {
                        if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.reset)
                        {
                            AlarmData.stVal = AlarmStateKind.norm;
                        }
                        }
                        break;
                    }
                }
                //-------ALARM-WITH-ACK-AND-LATCH-------
                if (AlarmData.almModel == AlarmModelKind.alarm_with_ack_and_latch)
                {
                    switch (AlarmData.stVal)
                    {
                    case AlarmStateKind.norm: //8
                        if (process_state.dtcng == "on")
                        {
                        AlarmData.stVal = AlarmStateKind.unack;
                        }
                        break;
                    case AlarmStateKind.unack: //15
                        if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.acknowledge)
                        {
                        AlarmData.stVal = AlarmStateKind.acked;
                        }
                        if (process_state.dtcng == "off")
                        {
                        AlarmData.stVal = AlarmStateKind.lunack;
                        }

                        break;
                    case AlarmStateKind.acked: //11
                        if (process_state.dtcng == "off")
                        {
                        AlarmData.stVal = AlarmStateKind.lacked;
                        }
                        break;

                    case AlarmStateKind.lunack: //12
                        if (process_state.dtcng == "on")
                        {
                        AlarmData.stVal = AlarmStateKind.unack;
                        }
                        else
                        {
                        if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.reset)
                        {
                            AlarmData.stVal = AlarmStateKind.rtnun;
                        }
                        else
                        {
                            if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.acknowledge)
                            {
                            AlarmData.stVal = AlarmStateKind.lacked;
                            }
                        }
                        }
                        break;

                    case AlarmStateKind.rtnun: //12
                        if (process_state.dtcng == "on")
                        {
                        AlarmData.stVal = AlarmStateKind.unack;
                        }
                        else
                        {
                        if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.acknowledge)
                        {
                            AlarmData.stVal = AlarmStateKind.norm;
                        }
                        }
                        break;

                    case AlarmStateKind.lacked: //12
                        if (process_state.dtcng == "on")
                        {
                        AlarmData.stVal = AlarmStateKind.acked;
                        }
                        else
                        {
                        if (AlarmData.Oper.ctlVal == AlarmCtlValueKind.reset)
                        {
                            AlarmData.stVal = AlarmStateKind.norm;
                        }
                        }
                        break;
                    }
                }
                process_state.dtcng = "none";
                AlarmData.Oper.ctlVal = "undefined";
                var msgout = msg;

                msgout.payload = {Item : this.name , Type : "int32", Value : {"stVal" : AlarmData.stVal}};

                node.send(msgout);
            }
        });
    }
    RED.nodes.registerType("alm-state-machine",AlmStateMachine);
}
