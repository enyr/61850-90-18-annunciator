<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output method="html"/>
<xsl:template match="/">
   <thead>
     <tr>
      <th>Time of Entry</th>
      <th>State</th>
      <th>Alarm</th>
      <th>Alarm Message</th>
      <th>Action</th>
      <th></th>
      <th>Shelf</th>
      <th>OOS</th>
    </tr>
   </thead>
   <tbody>
    <xsl:for-each select="Node/DOI">
     <xsl:variable name="node" select="../@name" /> 
     <xsl:variable name="pos" select="position()" /> 
    <tr>
	  <xsl:attribute name="id">			
			<xsl:value-of select="$node"/>
			<xsl:text>.GrAlm</xsl:text>
			<xsl:value-of select="format-number($pos, '000' )"/>
	  </xsl:attribute>
      <td></td>
      <td></td>
      <td><xsl:text>GrAlm</xsl:text><xsl:value-of select="format-number($pos, '000' )"/></td>
      <td><xsl:value-of select="almMsg" /></td>
      <td><xsl:value-of select="almAction" /></td>
      <td><!-- 0:acknowledge order-->
                 <xsl:element name="input">
                  <xsl:attribute name="type">submit</xsl:attribute>
                  <xsl:attribute name="value">Ack</xsl:attribute>
                 <xsl:attribute name="onclick">
			<xsl:text>CtlAlarm('</xsl:text>
			<xsl:value-of select="$node"/>
			<xsl:text>.GrAlm</xsl:text>
			<xsl:value-of select="format-number($pos, '000' )"/>
 			<xsl:text>',0)</xsl:text>
                  </xsl:attribute>
                 </xsl:element>
                <!-- 1:reset order-->
               <xsl:element name="input">
                 <xsl:attribute name="type">submit</xsl:attribute>
                 <xsl:attribute name="value">Rst</xsl:attribute>
                 <xsl:attribute name="onclick">
		 	<xsl:text>CtlAlarm('</xsl:text>
			<xsl:value-of select="$node"/>
			<xsl:text>.GrAlm</xsl:text>
			<xsl:value-of select="format-number($pos, '000' )"/> 
			 <xsl:text>',1)</xsl:text>
		</xsl:attribute>
                </xsl:element>
      </td>
      <td><!-- 2:shelve order -->
                <xsl:element name="input">
                  <xsl:attribute name="type">submit</xsl:attribute>
                  <xsl:attribute name="value">I</xsl:attribute>
                 <xsl:attribute name="onclick">
			<xsl:text>CtlAlarm('</xsl:text>
			<xsl:value-of select="$node"/>
			<xsl:text>.GrAlm</xsl:text>
			<xsl:value-of select="format-number($pos, '000' )"/> 
			<xsl:text>',2)</xsl:text>
		</xsl:attribute>
                 </xsl:element>
               <!-- 3:unshelve order -->
               <xsl:element name="input">
                 <xsl:attribute name="type">submit</xsl:attribute>
                 <xsl:attribute name="value">O</xsl:attribute>
                 <xsl:attribute name="onclick">
			<xsl:text>CtlAlarm('</xsl:text>
			<xsl:value-of select="$node"/>
			<xsl:text>.GrAlm</xsl:text>
			<xsl:value-of select="format-number($pos, '000' )"/> 
			<xsl:text>',3)</xsl:text>
		</xsl:attribute>
                </xsl:element>
      </td>
      <td><!-- 4:set-out-of-service order -->
                <xsl:element name="input">
                  <xsl:attribute name="type">submit</xsl:attribute>
                  <xsl:attribute name="value">I</xsl:attribute>
                 <xsl:attribute name="onclick">
			<xsl:text>CtlAlarm('</xsl:text>
			<xsl:value-of select="$node"/>
			<xsl:text>.GrAlm</xsl:text>
			<xsl:value-of select="format-number($pos, '000' )"/>
			 <xsl:text>',4)</xsl:text>
		</xsl:attribute>
                 </xsl:element>
               <!-- 5:set-in-Service order -->
               <xsl:element name="input">
                 <xsl:attribute name="type">submit</xsl:attribute>
                 <xsl:attribute name="value">O</xsl:attribute>
                 <xsl:attribute name="onclick">
			<xsl:text>CtlAlarm('</xsl:text>
			<xsl:value-of select="$node"/>
			<xsl:text>.GrAlm</xsl:text>
			<xsl:value-of select="format-number($pos, '000' )"/>
			 <xsl:text>',5)</xsl:text>
		</xsl:attribute>
                </xsl:element>
      </td>
    </tr>
    </xsl:for-each>
   </tbody>
 </xsl:template>
</xsl:stylesheet>